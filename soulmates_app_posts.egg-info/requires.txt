django >1.9, <2.0
cloudinary==1.4.0
django-taggit==0.20.1
FeinCMS==1.12.1
django-wysiwyg-redactor==0.4.9