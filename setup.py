from setuptools import setup, find_packages


setup(
    name="soulmates-app-posts",
    version='0.1',
    description='posts app modules etc',
    include_package_data = True,
    
    # Author details
    author='Eddy Lazar',
    author_email='eddy.lazar@soulmates.pro',
    license='MIT',
    packages=find_packages(),
    
    install_requires=[
        'django >1.9, <2.0',
        'cloudinary==1.4.0',
        'django-taggit==0.20.1',
        'FeinCMS==1.12.1',
        'django-wysiwyg-redactor==0.4.9'
    ],
    zip_safe=False
)