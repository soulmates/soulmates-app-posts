from django.contrib import admin

from django.utils.translation import ugettext_lazy as _
from soulmates_app_common.admin import get_admin_thumb, SEO_ADMIN_FIELDS
# Register your models here.



class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'published', get_admin_thumb, 'created_at')
    list_editable = ('published', )
    date_hierarchy = 'created_at'
    prepopulated_fields = {"slug": ("title",)}
    
    fieldsets = (
        SEO_ADMIN_FIELDS,
        (None, {
            'fields': ('title', 'slug', 'subtitle', 'published', 'image', 'text', 'tags')
        }),
    )
# example
# from .models import Post
# admin.site.register(Post, PostAdmin)