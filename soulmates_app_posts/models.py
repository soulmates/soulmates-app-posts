from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.db import models
from cloudinary.models import CloudinaryField
from redactor.fields import RedactorField
from django.utils.encoding import python_2_unicode_compatible
from taggit.managers import TaggableManager
from feincms.apps import app_reverse
from soulmates_app_common.models import SeoFieldsMixin



@python_2_unicode_compatible  # only if you need to support Python 2
class PostAbstract(SeoFieldsMixin, models.Model):
    # url config
    url_pattern_name = None
    urls_path = None
    
    # fields
    title = models.CharField(max_length=255, verbose_name=_('title'))
    slug = models.SlugField()
    subtitle = models.CharField(max_length=255, 
                                verbose_name=_('subtitle'),
                                null=True,
                                blank=True)
    image = CloudinaryField(_('image'), blank=True, null=True)
    text = RedactorField(
        _('article text'), 
        allow_file_upload=False,
        allow_image_upload=False,
        blank=True,
        null=True
        )
    tags = TaggableManager(blank=True)
    published = models.BooleanField(default=True, verbose_name=_('published'))
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_('created at'))
    updated_at = models.DateTimeField(auto_now=True, verbose_name=_('update at'))
        
    # methods
    def __str__(self):
        return self.title
        
    def get_absolute_url(self):
          return app_reverse(self.url_pattern_name, self.urls_path, kwargs={
              'slug': self.slug,
           })
        
    class Meta:
        verbose_name = _('Post')
        verbose_name_plural = _('Posts')
        ordering = ['-created_at']
        abstract = True



# class Post(PostAbstract):
#     # url config
#     url_pattern_name = 'post_detail'
#     urls_path = 'posts.urls'