from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.template.loader import render_to_string
from taggit.models import Tag

# @model {Model Class} - inherite from PostAbstract
class PostsContentType(models.Model):
    def render(self, **kwargs):
        if not hasattr(self, 'model'):
            raise ValueError('No model defined in PostsContentType')    
            
        tag_slug = kwargs['request'].GET.get('tag')
        posts = self.model.objects.filter(published=True)
        if tag_slug:
            posts = posts.filter(tags__name__in=[tag_slug])
        
        return render_to_string('posts/posts.html', {
            'posts': posts,
            })
            
    class Meta:
        abstract = True
        verbose_name = _('posts block')
        verbose_name_plural = _('posts block')
        

# @start_from {int} - slice query from
# @end_to {int} - slice query to
class RecentPostsContentType(models.Model):
    start_from = 0
    end_to = 3
    
    def render(self, **kwargs):
        if not hasattr(self, 'model'):
            raise ValueError('No model defined in PostsContentType')    
            
        posts = self.model.objects.filter(published=True)[self.start_from : self.end_to]
            
        return render_to_string('posts/recent.html', {
            'posts': posts,
            })
            
    class Meta:
        abstract = True
        verbose_name = _('recent posts block')
        verbose_name_plural = _('recent posts block')
        
        
class PostTagsContentType(models.Model):
    def render(self, **kwargs):
        return render_to_string('posts/tags.html', {
            'tags': Tag.objects.all()
            })
            
    class Meta:
        abstract = True
        verbose_name = _('tag')
        verbose_name_plural = _('tags')